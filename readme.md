# Code Assessment

## Tasks:

1. Create a webpage with Javascript that (using ajax) gets a JSON document (test_feed.json) and builds a data structure that captures the count of each word in the content of the "bodyHtml" field(s).

2. Utilize the Chart.js library (http://www.chartjs.org/) to visualize the results. You're free to implement it in anyway you see fit, including the use of other libraries.

### Time Limit: 4 hours

Please try to complete this exercise within four hours. If you're not able to complete it by then, please provide me with a link to your work along with a list of challenges, reasons they were a challenge, and how you would overcome them if you had more time. It's more important to see your process and approach than production-ready code (though it doesn't hurt).

I'll be looking for correctness, thoughtfulness, efficiency, and intent. The goal is to demonstrate your abilities in parsing and manipulating data as well as your ability to use a third-party library.